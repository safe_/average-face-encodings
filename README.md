Average face encodings
---------

An MVP of a project to calculate the average face encodings across a dataset of images
using the face-detection python library.

-----------------

Architecture
---
This is a cloud implementation that uses AWS lambda to compute the face encodings of images 
available on S3.  
The architecture is a lambda that enumerates a given bucket and streams object keys into SQS which in turn triggers
another lambda that calculates face encodings in the images and streams the results into another SQS queue which would 
sink into whatever path the rest of th system needs.

The project uses AWS SAM for build/deployment.

Installation and running
---
1. Minimal network setup: a VPC with a subnet and an S3 VPC-endpoint. And an execution role for the lambdas that has access
to S3 and SQS.
2. Install and configure AWS SAM with an admin user
3. Make sure you have python 3.8, pip and docker installed and available
4. Invoke `sam build`
5. Invoke `sam deploy`


Future work
-------
- Cost comparison of the different deployment approaches to this code. The first 
   being AWS Lambdas that compute encodings on the CPU and second being using the GPU
   implementation on EC2 machines.
- Tighten the used IAM roles to least-needed-privileges
- Setup continuous deployment for the project
