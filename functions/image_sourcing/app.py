from typing import List
from S3Service import S3Service
import structlog
import os
import boto3


logger = structlog.get_logger().bind()


def enumerate_objects(bucket_name: str, s3: S3Service) -> List[str]:
    # A method to enumerate an entire bucket
    object_key_names = []
    continuation_token = None
    while True:
        list_objects_result = s3.enumerate_bucket(bucket_name, continuation_token)

        object_key_names.extend(list_objects_result[0])
        continuation_token = list_objects_result[1]

        if list_objects_result[1] is None:
            break

    return object_key_names


def post_keys_to_sqs(keys: List[str]) -> None:
    # A method to post a list of strings (S3 Object keys, in this case) to SQS
    #       each item in a separate message
    queue_name = os.environ.get("QueueName")
    logger.info(f"Inserting them in queue {queue_name}", QueueName=queue_name)

    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName=queue_name)

    for k in keys:
        queue.send_message(MessageBody=k)


def lambda_handler(_, __):
    bucket_name = os.environ.get("BucketName")
    region = os.environ.get("Region")

    logger.info(f"Enumerating bucket {bucket_name} in region {region}", BucketName=bucket_name, Region=region)

    s3 = S3Service(region)
    object_key_names = enumerate_objects(bucket_name, s3)
    logger.info(f"Got {len(object_key_names)} objects in the bucket", ObjectCount=len(object_key_names))

    post_keys_to_sqs(object_key_names)
    logger.info("Finished inserting messages")

