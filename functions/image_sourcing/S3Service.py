import boto3
from typing import Optional, Tuple, List


class S3Service(object):

    def __init__(self, region_name: str):
        self._s3_client = boto3.client("s3", region_name=region_name)

    def enumerate_bucket(self, bucket_name: str, continuation_token: Optional[str] = None) -> Tuple[List[str], Optional[str]]:
        # Given a bucket name and a continuation token, return a tuple(list of object keys, next continuation token)
        # More on continuation tokens https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/s3.html#S3.Client.list_objects_v2
        if continuation_token is None:
            list_objects_result = self._s3_client.list_objects_v2(Bucket=bucket_name)
        else:
            list_objects_result = self._s3_client.list_objects_v2(Bucket=bucket_name, ContinuationToken=continuation_token)

        objects = [o["Key"] for o in list_objects_result["Contents"]]
        return objects, list_objects_result.get("NextContinuationToken", None)

