import face_recognition
from typing import List


class Encoder(object):
    @classmethod
    def calculate_encodings(cls, file_path: str) -> List[List[float]]:
        # Given a file path of an image, absolute or relative from cwd, calculate and return
        #      face encodings for all detected faces
        image = face_recognition.load_image_file(file_path)
        image_encodings = face_recognition.face_encodings(image)
        return [ndarr.tolist() for ndarr in image_encodings]

