import EncodedImage
from Encoder import Encoder
from S3Service import S3Service

import os
import tempfile
import shutil
import structlog
import json
from typing import List, Tuple, Dict

import boto3

logger = structlog.get_logger().bind()


def get_and_calculate_image(key: str, region: str, bucket_name: str) -> List[List[float]]:
    # A method that given an S3 object details (bucket, region, key), downloads the image file and calculates face
    #       encodings for all detected faces in the image
    s3 = S3Service(region)
    temp_dir_path = tempfile.mkdtemp()
    file_path = os.path.join(temp_dir_path, key)
    s3.save_object_to_file(bucket_name, key, file_path)
    result = Encoder.calculate_encodings(file_path)
    shutil.rmtree(temp_dir_path)
    return result


def get_encodings(keys: List[str], region: str, bucket: str) -> Tuple[List[EncodedImage.EncodedImage], List[str]]:
    # A method that given a list of S3 object keys, and their bucket and region, downloads all keys
    #   (assumed to be images) and returns a tuple of:
    #   [list of EncodedImage containing image key and image face encodings, list of failed keys]

    logger.info(f"Working on bucket {bucket} in region {region}", BucketName=bucket, Region=region)

    failed_keys = []
    face_encodings = []
    for key in keys:
        try:
            current_encodings = get_and_calculate_image(key, region, bucket)
            new_entry = EncodedImage.EncodedImage(key, current_encodings)
            face_encodings.append(new_entry)
        except Exception:
            failed_keys.append(key)
            logger.exception("Failed to process an image", key=key, exc_info=True)

    return face_encodings, failed_keys


def publish_encodings(face_encodings: List[EncodedImage.EncodedImage]) -> None:
    if len(face_encodings) == 0:
        return

    # A method that given a list of face encodings, serializes them to json and publishes them to SQS
    results_queue_name = os.environ.get("ResultsQueueName")
    logger.info(f"Publishing results to Queue {results_queue_name}")

    sqs = boto3.resource('sqs')
    queue = sqs.get_queue_by_name(QueueName=results_queue_name)

    serialized_encodings = json.dumps(face_encodings, cls=EncodedImage.EncodedImageEncoder)
    queue.send_message(MessageBody=serialized_encodings)


def lambda_handler(event, _):
    logger.info(f"Processing {len(event['Records'])} queue messages")

    object_keys = [r["body"] for r in event["Records"]]

    bucket_name = os.environ.get("BucketName")
    region = os.environ.get("Region")
    face_encodings, failed_keys = get_encodings(object_keys, region, bucket_name)

    try:
        publish_encodings(face_encodings)
    except Exception:
        logger.exception("Failed to publish to SQS")
        failed_keys = object_keys

    return {"batchItemFailures": [{"itemIdentifier": k} for k in failed_keys]}

