import boto3
from typing import Optional


class S3Service(object):

    def __init__(self, region_name: str):
        self._s3_client = boto3.client("s3", region_name=region_name)

    def save_object_to_file(self, bucket: str, key: str, filepath: str) -> None:
        # Downloads the object and saves to the filepath passed
        self._s3_client.download_file(bucket, key, filepath)

