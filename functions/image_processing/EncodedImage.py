from typing import List
import json


class EncodedImage(object):
    # A class to represent a face encoded image. Has the S3 key and the face_encodings list
    def __init__(self, key: str, face_encodings: List[List[float]]):
        self.key = key
        self.face_encodings = face_encodings


class EncodedImageEncoder(json.JSONEncoder):
    # A helper to serialize EncodedImage
    def default(self, o: EncodedImage):
        return {"key": o.key, "encodings": o.face_encodings}

